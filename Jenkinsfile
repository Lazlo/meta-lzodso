/*
 * Jenkinsfile - Delarative Pipeline for Building Yocto Project Poky
 */

@Library('pipeline-library') _

pipeline {
    options {
        buildDiscarder(logRotator(numToKeepStr: '5'))
    }
    agent {
        node {
            label '!master && linux && debian && sudo'
            // Use a custom workspace without '@' and '%' character in the path.
            // BitBake QA scripts to not like funny characters in the path
            // to the build directory.
            customWorkspace "${JOB_NAME.replace('%', '')}/build_${BUILD_NUMBER}"
        }
    }
    environment {
        APT_LOCK = "apt-lock-${env.NODE_NAME}"
        CLEAN_CMD = 'rm -rf poky'
    }
    stages {
        stage('Setup') {
            steps {
                timeout(time: 10, unit: 'MINUTES') {
                    retry(5) {
                        lock("${env.APT_LOCK}") {
                            echo 'Setting up ...'
                            sh 'sudo ./setup.sh'
                        }
                    }
                }
            }
        }
        stage('Fetch') {
            steps {
                sh './fetch.sh'
            }
        }
        stage('Build') {
            matrix {
                axes {
                    axis {
                        name 'MACHINE'
                        values 'qemux86-64', 'beaglebone-yocto'
                    }
                    axis {
                        name 'IMAGE'
                        values 'core-image-minimal', 'core-image-full-cmdline'
                    }
                }
                stages {
                    stage('Build') {
                        environment {
                            TMP_BASE_DIR      = '/var/build/yocto'
                            DL_DIR            = '/var/build/yocto/downloads'
                            SSTATE_DIR        = '/var/build/yocto/sstate-cache'
                            ARCHIVE_ARTIFACTS = "poky/build-${MACHINE}-${IMAGE}/tmp/deploy/images/${MACHINE}/*"
                            BUILD_DIR         = "build-${MACHINE}-${IMAGE}"
                        }
                        steps {
                            echo 'Building ...'
                            sh 'mkdir -p $TMP_BASE_DIR'
                            sh '''set $BUILD_DIR
                            . ./lzodso-init-build-env
                            echo "About to build image $IMAGE for machine $MACHINE"
                            bitbake $IMAGE
                            '''
                        }
                        post {
                            success {
                                archiveArtifacts artifacts: env.ARCHIVE_ARTIFACTS
                            }
                        }
                    }
                }
            }
        }
    }
    post {
        always {
            sh '${CLEAN_CMD}'
            commonStepNotification()
        }
    }
}
