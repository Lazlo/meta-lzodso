# lzoDSO Yocto Layers

Contains multiple Yocto layers for the lzoDSO hardware.

 * **meta-lzodso-bsp** board-support layer
 * **meta-lzodso** application layer
 * **meta-lzolinux** disto layer

Depends on following layers

 * **meta-oe**
 * **meta-python**
 * **meta-qt5**

## Building

Before building make sure to execute the ```setup.sh``` script as root.
It will take care of installing all the build-time dependencies.

When done run ```fetch.sh``` as a regular user to clone the poky repository.
Then source the ```lzodso-init-build-env``` (a wrapper for ```oe-init-build-env```).
Finally call BitBake with ```$IMAGE``` (exported by `lzodso-init-build-env`).

```
sudo ./setup.sh
./fetch.sh
. ./lzodso-init-build-env
bitbake $IMAGE
```

Here is a short outline what the build script does.

 * source ```oe-init-build-env``` to create a build directory
 * adjust the build configuration (```conf/local.conf```)
   - set paths to ```downloads``` and ```sstate-cache``` directories to be outside of the build directory
   - set the package class to be ```deb```
   - set the target machine to ```beaglebone-yocto```
   - enable ccache
 * add layers from this repository to the build configration
 * run bitbake to build an image (default image to build is ```lzodso-image```)

When the build has finished the resulting files can be found in
the ```tmp/deploy/images/beaglebone-yocto``` directory within the build directory.

To create a bootable SD card, run ```create-sdcard.sh``` like this:

```
MACHINE=beaglebone-yocto
IMAGE=lzodso-image
sudo ./create-sdcard.sh /dev/mmcblk0 poky/build-$MACHINE $IMAGE $MACHINE
```

## Images

Lists the BitBake recipes for creating system images.

 * __lzodso-image__: standard image for use with lzoDSO (default when sourcing ```lzodso-init-build-env```)
 * __lzodso-image-dev__: ... used for development
 * __lzodso-image-full-cmdline__: ... used for development
 * __lzodso-image-minimal__: ... used for system-level development

In order to build one of them simply pass that name to the call of ```bitbake```.

```
. /lzodso-init-build-env
bitbake lzodso-image-full-cmdline
```

## Development Status

 * BSP Layer
   - [ ] wik file to create SD card for beaglebone
   - [ ] create 'machine' recipe for lzodso
   - [ ] get graphics to work on beaglebone
   - [ ] get ADC to work on beaglebone
   - [ ] get I2C to work on beaglebone
   - [ ] get RT kernel to build for beaglebone
 * Application Layer
   - [ ] automatically start lzoDSO application
   - [ ] write recipes for IceStorm FPGA toolchain
 * OS Layer
   - [ ] create distro config based on sato
