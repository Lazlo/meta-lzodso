#!/bin/bash

set -e
set -u

default_branch="zeus"
branch="${BRANCH:-$default_branch}"
git_clone_opts=""
git_clone_opts="$git_clone_opts --depth 1"
git_clone_opts="$git_clone_opts -b $branch"

git clone $git_clone_opts git://git.yoctoproject.org/poky
cd poky
git clone $git_clone_opts git://git.openembedded.org/meta-openembedded
git clone $git_clone_opts git://github.com/meta-qt5/meta-qt5.git
cd -
