#!/bin/bash

apt-get -q install -y gawk wget git-core diffstat unzip texinfo \
gcc-multilib build-essential chrpath socat libsdl1.2-dev xterm \
python-setuptools python-crypto ccache cpio locales

# Make sure locales are installed and generated.
# Otherwise you will run into this message:
# "    Your system needs to support the en_US.UTF-8 locale."
echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && locale-gen
