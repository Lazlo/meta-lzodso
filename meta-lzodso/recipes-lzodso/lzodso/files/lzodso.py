#!/usr/bin/env python3

from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtWidgets import QLabel

class LzoDso(QWidget):

	def __init__(self):
		super().__init__()
		self.initUI()

	def initUI(self):
		self._layout = QVBoxLayout()
		self.setLayout(self._layout)
		self.testme = QLabel("lzoDSO-placeholder")
		self._layout.addWidget(self.testme)

if __name__ == '__main__':
	app = QApplication([])
	win = LzoDso()
	win.show()
	app.exec_()
