SUMMARY = "Digital Storage Oscilloscope Application"

LICENSE = "CLOSED"

SRC_URI = "file://lzodso.py"

RDEPENDS_${PN} = " \
    python3-pyqt5 \
    "

do_install() {
    install -d ${D}/usr/share/lzodso
    install -m 0755 ${WORKDIR}/lzodso.py ${D}/usr/share/lzodso
}
